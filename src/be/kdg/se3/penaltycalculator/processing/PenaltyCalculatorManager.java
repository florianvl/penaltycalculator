package be.kdg.se3.penaltycalculator.processing;

import be.kdg.se3.penaltycalculator.calculation.PenaltyCalculatorFormula;
import be.kdg.se3.penaltycalculator.communication.outgoing.RabbitMqPenaltySender;
import be.kdg.se3.penaltycalculator.communication.ViolationConverter;
import be.kdg.se3.penaltycalculator.domain.DTO.Penalty;
import be.kdg.se3.penaltycalculator.domain.exceptions.CommunicationException;
import be.kdg.se3.penaltycalculator.domain.model.NumberplateHistory;
import be.kdg.se3.penaltycalculator.domain.model.PenaltySettings;
import be.kdg.se3.penaltycalculator.domain.model.Violation;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class is responsible for processing incoming Violations, retrieving the amount to be paid and sending that to the outgoing queue.
 */

public class PenaltyCalculatorManager implements NewViolationListener {
    private static Logger logger = LogManager.getLogger(PenaltyCalculatorManager.class);
    private ViolationConverter xmlConverter;
    private PenaltyCalculatorFormula penaltyCalculator;
    private PenaltySettingsManager penaltySettingsManager;
    private ViolationBuffer violationBuffer;
    private RabbitMqPenaltySender rabbitMqPenaltySender;

    public PenaltyCalculatorManager(ViolationConverter xmlConverter, PenaltyCalculatorFormula calculator, PenaltySettingsManager penaltySettingsManager, RabbitMqPenaltySender sender, ViolationBuffer buffer) {
        this.xmlConverter = xmlConverter;
        this.penaltyCalculator = calculator;
        this.penaltySettingsManager = penaltySettingsManager;
        this.rabbitMqPenaltySender = sender;
        this.violationBuffer = buffer;
    }

    @Override
    public void processNewViolation(String violationString) {

        Violation violation = xmlConverter.StringToViolation(violationString);
        boolean buffered = violationBuffer.isBuffered(violation);

        if (!buffered) {
            PenaltySettings settings = penaltySettingsManager.getPenaltySettings();
            NumberplateHistory plateHistory = penaltySettingsManager.getPlateHistory(violation.getNumberPlate());
            double amountToPay = penaltyCalculator.calculatePenalty(violation, settings, plateHistory.getAmountOfPastPenalties());
            Penalty penalty = new Penalty(violation.getNumberPlate(), amountToPay, violation.getTimestamp());
            logger.log(Level.INFO, "Penalty: " + penalty.toString());

            try {
                rabbitMqPenaltySender.sendCalculatedPenalty(penalty);
            } catch (CommunicationException e) {
                logger.log(Level.ERROR, "Error occurred when putting calculated penalty on outgoing queue! No penalty is sent. ");
            }

        }


        logger.log(Level.INFO, "--------------------------------------");
    }
}
