package be.kdg.se3.penaltycalculator.processing;

import be.kdg.se3.penaltycalculator.domain.model.Violation;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;

/**
 * This class is responsible for buffering incoming Violations.
 */
public class ViolationBuffer {
    private static Logger logger = LogManager.getLogger(ViolationBuffer.class);

    private HashMap<String, Long > bufferedViolations;
    private long violationBufferInMs;
    private long flushViolationThreshold;

    public ViolationBuffer(long violationBufferInSeconds, long flushViolationThreshold) {
        bufferedViolations = new HashMap<>();
        this.violationBufferInMs = violationBufferInSeconds * 1000;
        this.flushViolationThreshold = flushViolationThreshold * 1000;
    }

    public boolean isBuffered(Violation newViolation){
        boolean isBuffered = false;
        if(bufferedViolations.containsKey(newViolation.getKeyString())){
            if (System.currentTimeMillis() - bufferedViolations.get(newViolation.getKeyString()) < violationBufferInMs){
                isBuffered = true;
            }

        }
        bufferedViolations.put(newViolation.getKeyString(), System.currentTimeMillis());
        return isBuffered;
    }

    public void flushBufferedViolations(){
        bufferedViolations.entrySet().removeIf(entry -> System.currentTimeMillis() - entry.getValue() > flushViolationThreshold);
    }

}
