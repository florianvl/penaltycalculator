package be.kdg.se3.penaltycalculator.processing;

import be.kdg.se3.penaltycalculator.domain.exceptions.CommunicationException;

import java.io.IOException;

/**
 * An interface for reacting to new incoming Violations.
 */

public interface NewViolationListener  {
    void processNewViolation(String violationString);
}
