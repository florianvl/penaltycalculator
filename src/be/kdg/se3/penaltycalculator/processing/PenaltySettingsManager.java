package be.kdg.se3.penaltycalculator.processing;

import be.kdg.se3.penaltycalculator.adapters.PenaltyServiceAdapter;
import be.kdg.se3.penaltycalculator.domain.exceptions.NumberplateHistoryException;
import be.kdg.se3.penaltycalculator.domain.exceptions.ServiceProxyException;
import be.kdg.se3.penaltycalculator.domain.model.NumberplateHistory;
import be.kdg.se3.penaltycalculator.domain.model.PenaltySettings;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class is responsible for providing default or cached PenaltySettings and refreshing these settings at the right times,
 * and providing the plate history for specific number plates and trying again if the call fails.
 */

public class PenaltySettingsManager {
    private static Logger logger = LogManager.getLogger(PenaltyServiceAdapter.class);
    private PenaltyServiceAdapter serviceAdapter;

    private PenaltySettings cachedSettings;
    private int settingsRefreshRateInSeconds;
    private long lastSettingsRefreshTime;
    private int numberPlateTries;
    private int numberPlateTriesInterval;

    public PenaltySettingsManager(PenaltyServiceAdapter serviceAdapter, PenaltySettings defaultPenaltySettings, int settingsRefreshRateInSeconds, int numberPlateTriesInterval, int numberPlateTries) {
        this.serviceAdapter = serviceAdapter;
        this.cachedSettings = defaultPenaltySettings;
        this.settingsRefreshRateInSeconds = settingsRefreshRateInSeconds;
        this.numberPlateTries = numberPlateTries;
        this.numberPlateTriesInterval = numberPlateTriesInterval;
    }


    public PenaltySettings getPenaltySettings() {
        long msSinceLastRefresh = System.currentTimeMillis() - lastSettingsRefreshTime;

        if ((msSinceLastRefresh / 1000) < settingsRefreshRateInSeconds) {
            return cachedSettings;
        } else { //refresh penalty settings

            try {
                cachedSettings = serviceAdapter.getPenaltySettings();
                lastSettingsRefreshTime = System.currentTimeMillis();
                logger.log(Level.INFO, "Refreshed Penalty Settings");
            } catch (ServiceProxyException e) {
                logger.log(Level.WARN, "Error occurred when retrieving PenaltySettings from the proxy service. Using cached or default settings.");
            }

            return cachedSettings;
        }
    }

    public NumberplateHistory getPlateHistory(String numberPlate) {
        NumberplateHistory plateHistory = new NumberplateHistory(numberPlate, 0);
        try {
            plateHistory = serviceAdapter.getPlateHistory(numberPlate);
        } catch (ServiceProxyException e) {
            //TODO wait and try again with plateHistory = getPlateHistory(numberPlate, 0);
            logger.log(Level.ERROR, "Error occured when getting plate history for " + numberPlate);
        }

        return plateHistory;
    }

    private NumberplateHistory getPlateHistory(String numberPlate, int amountOfTries) throws NumberplateHistoryException, InterruptedException {
        if (amountOfTries < numberPlateTries) {
            NumberplateHistory plateHistory;
            try {
                plateHistory = serviceAdapter.getPlateHistory(numberPlate);
                return plateHistory;
            } catch (ServiceProxyException e) {
                Thread.sleep(numberPlateTriesInterval);
                getPlateHistory(numberPlate, ++amountOfTries);
            }
        } else {
            throw new NumberplateHistoryException("Could not fetch number plate history. ");
        }

        return null;
    }


}
