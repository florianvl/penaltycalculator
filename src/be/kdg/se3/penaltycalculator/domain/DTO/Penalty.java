package be.kdg.se3.penaltycalculator.domain.DTO;

import java.time.LocalDateTime;

public class Penalty {
    private final String numberPlate;
    private final double penalty;
    private final LocalDateTime timestamp;

    public Penalty(String numberPlate, double penalty, LocalDateTime timestamp) {
        this.numberPlate = numberPlate;
        this.penalty = penalty;
        this.timestamp = timestamp;
    }

    public String getNumberPlate() {
        return numberPlate;
    }

    public double getPenalty() {
        return penalty;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return String.format("%s;%f;%s", getNumberPlate(), getPenalty(), getTimestamp().toString());
    }
}
