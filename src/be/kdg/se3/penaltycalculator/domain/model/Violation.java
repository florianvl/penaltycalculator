package be.kdg.se3.penaltycalculator.domain.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.time.LocalDateTime;

@XStreamAlias("Violation")
public class Violation {
    private final LocalDateTime timestamp;
    private final String numberPlate;
    private final String streetName;
    private final String cityName;

    public Violation(String numberPlate, String streetName, String cityName) {
        this.timestamp = LocalDateTime.now();
        this.numberPlate = numberPlate;
        this.streetName = streetName;
        this.cityName = cityName;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public String getNumberPlate() {
        return numberPlate;
    }

    public String getStreetName() {
        return streetName;
    }

    public String getCityName() {
        return cityName;
    }

    @Override
    public String toString() {
        return String.format("At %s, %s at %s in %s.", timestamp.toString(), numberPlate, streetName, cityName);
    }

    public String getKeyString(){
        return String.format("%s;%s", getNumberPlate(), getCityName());
    }
    @Override
    public boolean equals(Object o) {
        Violation otherViolation = (Violation) o;
        return this.getCityName().equals(otherViolation.getCityName()) && this.getNumberPlate().equals(otherViolation.getNumberPlate());
    }
}

