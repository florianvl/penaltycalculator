package be.kdg.se3.penaltycalculator.domain.model;

public class NumberplateHistory {
    private String plateId;
    private int amountOfPastPenalties;

    public NumberplateHistory(String plateId, int amountOfPastPenalties) {
        this.plateId = plateId;
        this.amountOfPastPenalties = amountOfPastPenalties;
    }

    public String getPlateId() {
        return plateId;
    }

    public int getAmountOfPastPenalties() {
        return amountOfPastPenalties;
    }

    @Override
    public String toString() {
        return String.format("Numberplate %s has %d past penalties", getPlateId(), getAmountOfPastPenalties());
    }
}
