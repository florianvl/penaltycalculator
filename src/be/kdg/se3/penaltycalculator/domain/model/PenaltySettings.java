package be.kdg.se3.penaltycalculator.domain.model;

public class PenaltySettings {
    private int speedFactor;
    private int emissionFactor;
    private int historyFactor;

    public PenaltySettings(int speedFactor, int emissionFactor, int historyFactor) {
        this.speedFactor = speedFactor;
        this.emissionFactor = emissionFactor;
        this.historyFactor = historyFactor;
    }

    public int getSpeedFactor() {
        return speedFactor;
    }

    public int getEmissionFactor() {
        return emissionFactor;
    }

    public int getHistoryFactor() {
        return historyFactor;
    }

    @Override
    public String toString() {
        return String.format("Speedfactor %d, Emissionfactor %d, Historyfactor %d", getSpeedFactor(), getEmissionFactor(), getHistoryFactor());
    }
}
