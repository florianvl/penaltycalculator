package be.kdg.se3.penaltycalculator.domain.exceptions;

/**
 * Wrapper for anything that goes wrong when communicating with the message queues.
 */
public class CommunicationException extends Exception {

    public CommunicationException(String message) {
        super(message);
    }

    public CommunicationException(String s, Throwable throwable) {
        super(s, throwable);
    }

}
