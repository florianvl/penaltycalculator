package be.kdg.se3.penaltycalculator.domain.exceptions;

/**
 * Wrapper for problems with fetching the history for a numberplate.
 */
public class NumberplateHistoryException extends Exception{
    public NumberplateHistoryException(String s) {
        super(s);
    }
}
