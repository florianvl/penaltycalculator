package be.kdg.se3.penaltycalculator.domain.exceptions;

/**
 * Wrapper for anything that goes wrong during communcating with the Penalty Service Proxy.
 */
public class ServiceProxyException  extends Exception {
    public ServiceProxyException(String message) {
        super(message);
    }

    public ServiceProxyException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
