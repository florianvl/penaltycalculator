package be.kdg.se3.penaltycalculator.calculation;

import be.kdg.se3.penaltycalculator.domain.model.PenaltySettings;
import be.kdg.se3.penaltycalculator.domain.model.Violation;

/**
 * Interface for calculating the penalty for a given Violation.
 */
public interface PenaltyCalculatorFormula {

    double calculatePenalty(Violation violation, PenaltySettings settings, int pastPenalties);
}
