package be.kdg.se3.penaltycalculator.calculation;

import be.kdg.se3.penaltycalculator.domain.model.PenaltySettings;
import be.kdg.se3.penaltycalculator.domain.model.Violation;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * An implementation of the PenaltyCalculatorFormula interface.
 * This class calculates penalties by taking past violations into account.
 * Formula: amount to pay = ((speed - max allowed speed) * speed factor) + (amount of past penalties * history factor)
 */

public class HistoryCalculatorFormula implements PenaltyCalculatorFormula {
    private static Logger logger = LogManager.getLogger(HistoryCalculatorFormula.class);

    private PenaltyCalculatorFormula normalPenaltyCalculator;

    public HistoryCalculatorFormula(PenaltyCalculatorFormula normalPenaltyCalculator){
        this.normalPenaltyCalculator = normalPenaltyCalculator;
    }

    @Override
    public double calculatePenalty(Violation violation, PenaltySettings settings, int pastPenalties) {

        double base = normalPenaltyCalculator.calculatePenalty(violation, settings, pastPenalties);
        double penalty = base + (pastPenalties * settings.getHistoryFactor());
        return penalty;
    }
}
