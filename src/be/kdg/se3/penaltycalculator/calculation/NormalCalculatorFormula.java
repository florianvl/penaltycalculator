package be.kdg.se3.penaltycalculator.calculation;

import be.kdg.se3.penaltycalculator.domain.model.EmissionViolation;
import be.kdg.se3.penaltycalculator.domain.model.PenaltySettings;
import be.kdg.se3.penaltycalculator.domain.model.SpeedViolation;
import be.kdg.se3.penaltycalculator.domain.model.Violation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * An implementation of the PenaltyCalculatorFormula interface.
 * This class calculates penalties without taking past violations into account.
 * Formula: amount to pay = (speed - max allowed speed) * speed factor
 */
public class NormalCalculatorFormula implements PenaltyCalculatorFormula {
    private static Logger logger = LogManager.getLogger(NormalCalculatorFormula.class);

    @Override
    public double calculatePenalty(Violation violation, PenaltySettings settings, int pastPenalties) {

        if (violation instanceof SpeedViolation) {
            SpeedViolation speedViolation = (SpeedViolation) violation;
            return (speedViolation.getMeasuredSpeed() - speedViolation.getMaxSpeed()) * settings.getSpeedFactor();
        }
        if(violation instanceof EmissionViolation){
            return settings.getEmissionFactor();
        }
        return -1;    }


}
