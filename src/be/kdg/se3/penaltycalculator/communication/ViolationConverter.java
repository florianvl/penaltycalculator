package be.kdg.se3.penaltycalculator.communication;

import be.kdg.se3.penaltycalculator.domain.model.Violation;

/**
 * An interface for conversion of json/xml/... Strings and Violations.
 */
public interface ViolationConverter {
    Violation StringToViolation(String violationString);
    String ViolationToString(Violation violationToConvert);
}
