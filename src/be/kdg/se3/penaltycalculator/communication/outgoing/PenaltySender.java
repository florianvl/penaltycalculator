package be.kdg.se3.penaltycalculator.communication.outgoing;

import be.kdg.se3.penaltycalculator.domain.DTO.Penalty;
import be.kdg.se3.penaltycalculator.domain.exceptions.CommunicationException;

/**
 * Interface for sending the calculated penalties to the billing department.
 */
public interface PenaltySender {
    void sendCalculatedPenalty(Penalty penalty) throws CommunicationException;
}
