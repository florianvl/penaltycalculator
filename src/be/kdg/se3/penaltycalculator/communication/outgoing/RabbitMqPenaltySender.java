package be.kdg.se3.penaltycalculator.communication.outgoing;

import be.kdg.se3.penaltycalculator.domain.DTO.Penalty;
import be.kdg.se3.penaltycalculator.domain.exceptions.CommunicationException;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * This class is responsible for sending the calculated penalties on a RabbitMq messaging queue.
 */

public class RabbitMqPenaltySender implements PenaltySender {
    private static Logger logger = LogManager.getLogger(RabbitMqPenaltySender.class);

    private String queueName;
    private ConnectionFactory factory;
    private Connection connection;
    private Channel channel;

    public RabbitMqPenaltySender(String queueName, String host) throws CommunicationException {
        this.queueName = queueName;
        try {
            factory = new ConnectionFactory();
            factory.setHost(host);
            connection = factory.newConnection();
            channel = connection.createChannel();
            channel.queueDeclare(queueName, false, false, false, null);
            logger.log(Level.INFO, "RabbitMq connection successfully set up for queue " + queueName);
        } catch (IOException e) {
            throw new CommunicationException("Error occurred at RabbitMq channel setup.", e);
        } catch (TimeoutException e) {
            throw new CommunicationException("Error occurred when creating RabbitMq connection.", e);
        }
    }


    public void sendCalculatedPenalty(Penalty penalty) throws CommunicationException {
        try {
            channel.basicPublish("", queueName, null, penalty.toString().getBytes());
            logger.log(Level.INFO, "Penalty successfully sent on " + queueName);
        } catch (IOException e) {
            throw new CommunicationException("Error occured when publishing message on queue " + queueName, e);
        }
    }
}
