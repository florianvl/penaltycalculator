package be.kdg.se3.penaltycalculator.communication;
import be.kdg.se3.penaltycalculator.domain.model.EmissionViolation;
import be.kdg.se3.penaltycalculator.domain.model.SpeedViolation;
import be.kdg.se3.penaltycalculator.domain.model.Violation;
import com.thoughtworks.xstream.XStream;

/**
 * This class is responsible for the conversion between XML and Violation objects.
 */
public class XMLConverter implements ViolationConverter {
    private XStream xstream;

    public XMLConverter() {
        xstream = new XStream();
        xstream.ignoreUnknownElements();
        xstream.processAnnotations(Violation.class);
        xstream.processAnnotations(SpeedViolation.class);
        xstream.processAnnotations(EmissionViolation.class);
    }

    @Override
    public Violation StringToViolation(String violationString) {
        Violation violation = (Violation) xstream.fromXML(violationString);
        return violation;
    }

    @Override
    public String ViolationToString(Violation violationToConvert) {
        return xstream.toXML(violationToConvert);
    }
}
