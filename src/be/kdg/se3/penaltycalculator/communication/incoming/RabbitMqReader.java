package be.kdg.se3.penaltycalculator.communication.incoming;

import be.kdg.se3.penaltycalculator.domain.exceptions.CommunicationException;
import be.kdg.se3.penaltycalculator.processing.NewViolationListener;
import com.rabbitmq.client.*;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

/**
 * This class is responsible for reading Violation messages from the RabbitMq message queue
 * and notifying interested parties of newly read Violations.
 */
public class RabbitMqReader implements ViolationReader {
    private static Logger logger = LogManager.getLogger(RabbitMqReader.class);
    private List<NewViolationListener> listeners;

    private String queueName;
    private ConnectionFactory factory;
    private Connection connection;
    private Channel channel;

    public RabbitMqReader(String queueName, String host) throws CommunicationException {
        listeners = new ArrayList<>();
        this.queueName = queueName;
        try {
            factory = new ConnectionFactory();
            factory.setHost(host);
            connection = factory.newConnection();
            channel = connection.createChannel();
            channel.queueDeclare(queueName, false, false, false, null);
            logger.log(Level.INFO, "RabbitMq connection successfully set up for queue " + queueName);
        } catch (IOException e) {
            throw new CommunicationException("Error occurred at RabbitMq channel setup.", e);
        } catch (TimeoutException e) {
            throw new CommunicationException("Error occurred when creating RabbitMq connection.", e);
        }
    }

    @Override
    public void startPolling() throws CommunicationException {
        logger.info("Starting polling of RabbitMq queue " + queueName);
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String receivedXml = new String(body, "UTF-8");
                logger.log(Level.INFO, "Retrieved new violation!");
                notifyListeners(receivedXml);
            }
        };

        try {
            channel.basicConsume(queueName, true, consumer);
        } catch (IOException e) {
            throw new CommunicationException("Error occured when reading from RabbitMq queue " + queueName, e);

        }
    }

    @Override
    public void addListener(NewViolationListener listenerToAdd) {
        this.listeners.add(listenerToAdd);
        logger.log(Level.INFO, "New listener added to " + this.getClass());
    }

    @Override
    public void removeListener(NewViolationListener listenerToRemove) {
        this.listeners.remove(listenerToRemove);
        logger.log(Level.INFO, "Listener removed from " + this.getClass());
    }

    @Override
    public void notifyListeners(String receivedMessage) {
        if (this.listeners.isEmpty()) {
            logger.log(Level.WARN, "No listeners configured.");
        }
        for (NewViolationListener listener : listeners) {
            listener.processNewViolation(receivedMessage);
        }
    }

}

