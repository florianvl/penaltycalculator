package be.kdg.se3.penaltycalculator.communication.incoming;

import be.kdg.se3.penaltycalculator.domain.exceptions.CommunicationException;
import be.kdg.se3.penaltycalculator.processing.NewViolationListener;

import java.io.IOException;

/**
 * Interface for reading/receiving new Violations.
 */
public interface ViolationReader {
    void startPolling() throws CommunicationException;

    void addListener(NewViolationListener listenerToAdd);

    void removeListener(NewViolationListener listenerToRemove);

    void notifyListeners(String receivedMessage) throws IOException, CommunicationException;

}
