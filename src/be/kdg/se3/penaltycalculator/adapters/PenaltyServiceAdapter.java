package be.kdg.se3.penaltycalculator.adapters;

import be.kdg.se3.penaltycalculator.domain.exceptions.ServiceProxyException;
import be.kdg.se3.penaltycalculator.domain.model.NumberplateHistory;
import be.kdg.se3.penaltycalculator.domain.model.PenaltySettings;
import be.kdg.se3.services.PenaltyServiceProxy;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * This class is responsible for providing easy access to the Penalty Service Proxy.
 */

public class PenaltyServiceAdapter {
    private static Logger logger = LogManager.getLogger(PenaltyServiceAdapter.class);
    private final String PENALTY_SETTINGS_URL = "www.services4se3.com/penalty/settings";
    private final String PLATE_CHECK_URL = "www.services4se3.com/penalty/";

    private Gson gson;
    private PenaltyServiceProxy penaltyServiceProxy;

    public PenaltyServiceAdapter() {
        gson = new Gson();
        penaltyServiceProxy = new PenaltyServiceProxy();
    }

    public PenaltySettings getPenaltySettings() throws ServiceProxyException {
        String json;
        try {
            json = penaltyServiceProxy.get(PENALTY_SETTINGS_URL);
        } catch (IOException e) {
            throw new ServiceProxyException("Error occurred when retrieving penalty settings!", e);
        }
        PenaltySettings settings = gson.fromJson(json, PenaltySettings.class);

        return settings;
    }

    public NumberplateHistory getPlateHistory(String numberPlate) throws ServiceProxyException {
        String json;

        try {
            json = penaltyServiceProxy.get(PLATE_CHECK_URL + numberPlate);
        } catch (IOException e) {
            throw new ServiceProxyException("Error occured when getting plate history for \"" + numberPlate + "\"", e);
        }

        NumberplateHistory history = gson.fromJson(json, NumberplateHistory.class);
        return history;
    }


}
