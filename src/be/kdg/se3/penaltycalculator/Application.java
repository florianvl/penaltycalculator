package be.kdg.se3.penaltycalculator;
import be.kdg.se3.penaltycalculator.adapters.PenaltyServiceAdapter;
import be.kdg.se3.penaltycalculator.calculation.HistoryCalculatorFormula;
import be.kdg.se3.penaltycalculator.calculation.NormalCalculatorFormula;
import be.kdg.se3.penaltycalculator.calculation.PenaltyCalculatorFormula;
import be.kdg.se3.penaltycalculator.communication.outgoing.RabbitMqPenaltySender;
import be.kdg.se3.penaltycalculator.communication.ViolationConverter;
import be.kdg.se3.penaltycalculator.communication.XMLConverter;
import be.kdg.se3.penaltycalculator.domain.exceptions.CommunicationException;
import be.kdg.se3.penaltycalculator.domain.model.PenaltySettings;
import be.kdg.se3.penaltycalculator.processing.NewViolationListener;
import be.kdg.se3.penaltycalculator.processing.PenaltySettingsManager;
import be.kdg.se3.penaltycalculator.processing.PenaltyCalculatorManager;
import be.kdg.se3.penaltycalculator.communication.incoming.RabbitMqReader;
import be.kdg.se3.penaltycalculator.processing.ViolationBuffer;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class is responsible for the initialisation and wiring of all necessary objects
 * and starting the application by starting the polling for new violations.
 */
public class Application {
    private static String rabbitMqQueueIn = "ViolationQueue";
    private static String rabbitMqQueueOut = "PenaltyQueue";
    private static String rabbitMqHost = "localhost";
    private static PenaltySettings  defaultPenaltySettings = new PenaltySettings(4, 55, 7 );
    private static int penaltySettingsRefreshRateInSeconds = 60;
    private static int violationBufferInSeconds = 36000;
    private static int violationBufferFlushInSeconds = 1800;
    private static int numberplateFetchTries = 3;
    private static int numberplateFetchWaitTimeInSeconds = 3;
    private static Logger logger = LogManager.getLogger(Application.class);


    private static RabbitMqPenaltySender rabbitMqOut;
    private static RabbitMqReader rabbitMqReader;
    private static ViolationBuffer buffer;
    private static ViolationConverter xmlConverter;
    public static void main(String[] args) {

        try {
            xmlConverter = new XMLConverter();
            rabbitMqReader = new RabbitMqReader(rabbitMqQueueIn,  rabbitMqHost);
            rabbitMqOut = new RabbitMqPenaltySender(rabbitMqQueueOut, rabbitMqHost);
            buffer = new ViolationBuffer(violationBufferInSeconds, violationBufferFlushInSeconds);

            PenaltyCalculatorFormula normalCalculatorMethod = new NormalCalculatorFormula();
            PenaltyCalculatorFormula calculatorMethodToUse = new HistoryCalculatorFormula(normalCalculatorMethod);

            PenaltySettingsManager settingsManager = new PenaltySettingsManager(new PenaltyServiceAdapter(), defaultPenaltySettings, penaltySettingsRefreshRateInSeconds, numberplateFetchWaitTimeInSeconds, numberplateFetchTries);
            NewViolationListener listener = new PenaltyCalculatorManager(xmlConverter, calculatorMethodToUse, settingsManager, rabbitMqOut, buffer);

            rabbitMqReader.addListener(listener);
            rabbitMqReader.startPolling();

        } catch (CommunicationException e) {
            logger.log(Level.ERROR, e.getMessage());
        }



    }
}
